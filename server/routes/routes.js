const express = require("express");
const controllers = require("../controllers/controllers");
const router = express.Router();
router
    .route("/hae")
    .post(controllers.haeUrheilija);

router
    .route("/lisaa")
    .post(controllers.lisaaUrheilija);

router
    .route("/poista")
    .post(controllers.poista);

router
    .route("/paivita")
    .post(controllers.paivita);

module.exports = router;