const express = require('express');
const app = express();
const port = 3001;
app.use(express.json());
app.use('/api', require("./routes/routes"));
app.use((err,req,res,next)=>{
    console.log(err.stack);
    console.log();
    console.log();
    res.status(500).json({message:"Jotain meni vikaan"});
});
app.listen(port, () => {

    console.log("REST API käynnissä.");

});