const urheilijaDB = require("../configure/urheilijaDB");
class Urheilija{
    constructor(id,etunimi,sukunimi,kutsumanimi,syntymavuosi,paino,kuvalinkki,laji,saavutukset){
        this.id=id;
        this.etunimi=etunimi;
        this.sukunimi=sukunimi;
        this.kutsumanimi=kutsumanimi;
        this.syntymavuosi=syntymavuosi;
        this.paino=paino;
        this.kuvalinkki=kuvalinkki;
        this.laji=laji;
        this.saavutukset=saavutukset;
    }
    static etsi(urheilija){
        let ehtot = '';
        let hakuparametrit = {};
        for(let i=0; i < Object.keys(urheilija).length; i++){
            if(Object.values(urheilija)[i] != undefined){
                hakuparametrit[Object.keys(urheilija)[i]]= Object.values(urheilija)[i];
            }
        }
        if(Object.keys(hakuparametrit).length != 0){
        for(let i=0; i< Object.keys(hakuparametrit).length; i++){
            ehtot += `${Object.keys(hakuparametrit)[i]}="${Object.values(hakuparametrit)[i]}"`;
            if(i< Object.keys(hakuparametrit).length-1){ ehtot += " AND " }
        }

        let lause = `SELECT * FROM urheilija2 WHERE ${ehtot}`;
        let urheilijatiedot = urheilijaDB.execute(lause);
        

        /*.then((result)=>{
            saavutusLause = `SELECT kilpailunimi, aika, saavutus FROM saavutukset WHERE kilpailija_id=${result[0][0].id}`;
            result[0][0].saavutukset = urheilijaDB.execute(saavutusLause);
            return result;
        });*/

        return urheilijatiedot;
        } else { return []; }
    }
    static etsiSaavutukset(kilpailijaId){

        let lause = `SELECT kilpailunimi,aika,saavutus FROM saavutukset WHERE kilpailija_id=${kilpailijaId}`;
        return urheilijaDB.execute(lause);

    }
    async lisaa(){
        let lauseUrheilija = "INSERT INTO urheilija2 (etunimi,sukunimi,laji";
        let lauseSaavutukset = "INSERT INTO saavutukset (aika,saavutus,kilpailunimi) VALUES (";
        let lisaysparametritUrheilija = {};
        let loppuosaLauseestaUrheilija ='';
        if(this.etunimi != undefined
            && this.sukunimi != undefined
            && this.laji != undefined){
            for(let i=0; i < Object.keys(this).length; i++){
                if(Object.values(this)[i] != undefined 
                && Object.keys(this)[i] != "saavutukset"
                && Object.keys(this)[i] != "etunimi"
                && Object.keys(this)[i] != "sukunimi"
                && Object.keys(this)[i] != "laji"){
                    lisaysparametritUrheilija[Object.keys(this)[i]]= Object.values(this)[i];
                }
            }
            for(let i=0; i<Object.keys(lisaysparametritUrheilija).length;i++){
                lauseUrheilija += `,${Object.keys(lisaysparametritUrheilija)[i]}`;
                loppuosaLauseestaUrheilija += `,"${Object.values(lisaysparametritUrheilija)[i]}"`;
            }
            lauseUrheilija += `) VALUES ("${this.etunimi}","${this.sukunimi}","${this.laji}"`+loppuosaLauseestaUrheilija + ")";
        }

        // TODO: saavutusta varten tarvitaan kilpailija_id ja se on tietokannan urheilijan id
        if(this.saavutukset != undefined){
        if(this.saavutukset[0].aika != undefined
            && this.saavutukset[0].saavutus != undefined
            && this.saavutukset[0].kilpailunimi != undefined){
            lauseSaavutukset += `"${this.saavutukset[0].aika}","${this.saavutukset[0].saavutus}","${this.saavutukset[0].kilpailunimi}")`;
        }}
        // console.log(lauseUrheilija + "  "+lauseSaavutukset);
        const [lisays, _] = await urheilijaDB.execute(lauseUrheilija);
        // if(this.saavutukset != undefined){
        // const [saavutukset, __] = await urheilijaDB.execute(lauseSaavutukset);
        // }
        return this;
    }
    static poista(id){
        let lause = `DELETE FROM urheilija2 WHERE id=${id}`;
        return urheilijaDB.execute(lause);
    }
    async paivita(){
        let lause =
        `UPDATE urheilija2 SET etunimi="${this.etunimi}", sukunimi="${this.sukunimi}", kutsumanimi="${this.kutsumanimi}", syntymavuosi="${this.syntymavuosi}", painoKG="${this.paino}", kuvalinkki="${this.kuvalinkki}", laji="${this.laji}" WHERE id=${this.id}`;
        let tiedot = urheilijaDB.execute(lause);
        //urheilijaDB.execute("COMMIT");

        return tiedot;
    }
}
module.exports = Urheilija;