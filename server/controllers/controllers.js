const Urheilija = require("../models/Urheilija");
exports.haeUrheilija = async (req,res,next) =>{
    try {
        let {id,etunimi,sukunimi,kutsumanimi,
        syntymavuosi,paino,kuvalinkki,laji,
        saavutukset} = req.body;
        let urheilija = new Urheilija(id,etunimi,sukunimi,kutsumanimi,syntymavuosi,paino,
            kuvalinkki,laji,saavutukset);
        let [sporttiHeppu, _] = await Urheilija.etsi(urheilija);
        if(sporttiHeppu[0] != undefined){
             for(let i=0; i < sporttiHeppu.length; i++) {
                let [saavutuslista, __] = await Urheilija.etsiSaavutukset(sporttiHeppu[i].id);
                sporttiHeppu[i].saavutukset = saavutuslista;
             }
        }
        res.status(200).json({sporttiHeppu});
    } catch (error) {
        console.log(error);
        next(error);
    }
};
exports.lisaaUrheilija = async(req,res,next)=>{

    try {
        let {etunimi,sukunimi,kutsumanimi,
            syntymavuosi,paino,kuvalinkki,laji,
            saavutukset} = req.body;
        let urheilija = new Urheilija(etunimi,sukunimi,kutsumanimi,syntymavuosi,paino,
                kuvalinkki,laji,saavutukset);
        urheilija = await urheilija.lisaa();
        res.status(200).json({message:"tietue lisätty",urheilija})
    } catch (error) {
        console.log(error);
        next(error);
    }

};
exports.poista = async (req,res,next)=>{
    try {
        let id = req.body.id;
        console.log (id);
        let [poisto, _] = await Urheilija.poista(id);
        res.status(200).json({message:"tietue poistettu"});
    } catch (error) {
        console.log(error);
        next(error);
    }
};
exports.paivita = async (req,res,next)=>{
    try {
        let {id,etunimi,sukunimi,kutsumanimi,
            syntymavuosi,paino,kuvalinkki,laji,
            saavutukset} = req.body;
        let urheilija = new Urheilija(id,etunimi,sukunimi,kutsumanimi,syntymavuosi,paino,
                kuvalinkki,laji,saavutukset);
        urheilija = await urheilija.paivita();
        res.status(200).json({message:"tietue päivitetty", urheilija});
    } catch (error) {
        console.log(error);
        next(error);
    }
};