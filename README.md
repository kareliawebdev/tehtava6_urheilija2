# tehtava6_urheilija2

## Tehtävänanto

1) Toteuta JavaScript-palvelinsovellus, joka tarjoaa REST-rajapinnan kautta tiedon urheilijoista

Suunnittele ja toteuta express.js-kirjaston avulla REST-rajapinta, 
toteuta datan haku,lisäys, poisto ja päivitys mariadb-tietokantaan 
Tietokannassa oltava:
Nimet (etunimi, sukunimi, kutsumanimi),
Syntymävuosi (Date)
Paino (number)
www-linkki kuvaan,
Laji
Saavutukset
 (5 pistettä)

2) Toteuta React:lla käyttöliittymä, jonka avulla voit lukea sekä lisätä, päivittää ja poistaa urheilijoita tietokantaan käyttäen vaiheen 1 backendiä. Yritä käyttää arkkitehtuurina ContextAPI:ia sovellettuna funktionaalisiin komponentteihin. (5 pistettä)

3) Ota Bootstrap käyttöön käyttöliittymässä. Tyyli on vapaa ts. saat itse valita tarkemmin tyylin. (2 pistettä)

## Tehtävän osittelu

- [x] REST rajapinta
- [x] MariaDB säilömät tiedot
  - [x] etunimi, sukunimi, kutsumanimi
  - [x] syntymävuosi
  - [x] paino
  - [x] kuvalinkki
  - [x] laji
  - [x] saavutukset
- [x] MariaDB toiminnot
  - [x] haku
  - [x] lisäys
  - [x] poisto
  - [x] päivitys
- [ ] React käyttöliittymä
  - [ ] ContextAPI
- [ ] Bootsrap


## Käytetyt sovellukset ja versiot

Node.js 18
MariaDB 15

## Asennus ja käyttäminen

Tietokannan luomista varten databaseSkripti.sql -tiedosto.
