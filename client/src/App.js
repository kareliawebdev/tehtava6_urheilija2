import React from "react";
import logo from './logo.svg';
import './App.css';
import { BrowserRouter,Routes,Route } from "react-router-dom";
import Layout from "./components/Layout";
import Hae from "./components/Hae";
import Tietoa from "./components/Tietoa";

function App() {
  
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route path="tietoa" element={<Tietoa />}/>
        <Route path="hae" element={<Hae />} />
      </Route>
    </Routes>
    </BrowserRouter>
  );
}

export default App;
