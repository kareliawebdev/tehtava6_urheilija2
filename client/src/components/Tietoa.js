const Tietoa=()=>{
    return (
        <>
        <article>
            <h2>Tietoa palvelusta</h2>
            <p>Tämä palvelu on kehitystyön alla ja kaikkia
                toiminnallisuuksia ei ole vielä testattu.
                Tiedot voivat myös olla virheellisiä.
            </p>
        </article>
        </>
    );
}
export default Tietoa;