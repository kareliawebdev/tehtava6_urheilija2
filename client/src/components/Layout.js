import {Outlet, Link} from "react-router-dom";
const Layout=()=>{
    return (
        <>
        <div><h1>Urheilija tietopalvelu</h1></div>
        <nav>
          <div><Link to="/tietoa">Tietoja palvelusta</Link></div>
          <div><Link to="/hae">Hae urheilijoiden tietoja tieokannasta</Link></div>
        </nav>
        <Outlet />
        </>
    );
}
export default Layout;