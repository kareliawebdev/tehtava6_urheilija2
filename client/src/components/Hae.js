import { useState } from "react";
import Hakutulos from "./Hakutulos";
const Hae=()=>{
    const[syote, setSyote]=useState({});
    const kasitteleMuutos = (e) =>{
        const nimi = e.target.name;
        const arvo = e.target.value;
        setSyote(arvot=>({...arvot, [nimi]: arvo}));
    }
    const laheta = (e) =>{
        e.preventDefault();
        // post -> restapiin
        // vastaus contextiin... ehkä??
        // käsittely Hakutulos.js:ssä
        
    }
    return (
        <>
        <h2>Hae tietoja tietokannasta</h2>
        <form onSubmit={laheta}>
            <label for="etunimi">
                etunimi: 
                <input type="text" name="etunimi" value={syote.etunimi || ""} onChange={kasitteleMuutos}/></label><br/>
            
            <label for="sukunimi">
                sukunimi: 
                <input type="text" name="sukunimi" value={syote.sukunimi || ""} onChange={kasitteleMuutos}/></label><br/>
            
            <label for="kutsumanimi">
                kutsumanimi: 
                <input type="text" name="kutsumanimi" value={syote.kutsumanimi || ""} onChange={kasitteleMuutos}/></label><br/>
            
            <label for="syntymavuosi">
                syntymävuosi: 
                <input type="text" name="syntymavuosi" value={syote.syntymavuosi || ""} onChange={kasitteleMuutos}/></label><br/>
            
            <label for="paino">
                paino: 
                <input type="text" name="paino" value={syote.paino || ""} onChange={kasitteleMuutos}/></label><br/>

            <label for="laji">
                laji: 
                <input type="text" name="laji" value={syote.laji || ""} onChange={kasitteleMuutos}/></label><br/>
                
            <input type="submit"/>
            
        </form><br/>
        <Hakutulos />
        </>
        
    );
}
export default Hae;
//<input type="submit"/>