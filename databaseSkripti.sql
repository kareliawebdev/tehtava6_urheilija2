CREATE DATABASE IF NOT EXISTS urheilija2;
USE urheilija2;

CREATE TABLE urheilija2 (
    id INT NOT NULL AUTO_INCREMENT,
    etunimi VARCHAR(50) NOT NULL, 
    sukunimi VARCHAR(60) NOT NULL,
    kutsumanimi VARCHAR(50),
    syntymavuosi YEAR,
    painoKG FLOAT,
    kuvalinkki VARCHAR(80),
    laji VARCHAR(80) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO urheilija2 (etunimi,sukunimi,syntymavuosi,laji)
VALUES
('Seppo','Räty',1962,'Keihäänheitto'),
('Ilse','Lillak',1961,'Keihäänheitto'),
('Harri','Kirvesniemi',1958,'Maastohiihto'),
('Marja-Liisa','Kirvesniemi',1955,'Maastohiihto');

CREATE TABLE saavutukset (
    id INT NOT NULL AUTO_INCREMENT,
    aika DATE NOT NULL,
    saavutus VARCHAR(60) NOT NULL, 
    kilpailunimi VARCHAR(80) NOT NULL,
    kilpailija_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (kilpailija_id) REFERENCES urheilija2(id) ON DELETE CASCADE
);

INSERT INTO saavutukset (aika,saavutus,kilpailunimi,kilpailija_id)
VALUES
('1984-02-19','Kulta','Sarajevo 1984', (SELECT urheilija2.id FROM urheilija2 WHERE etunimi='Marja-Liisa')),
('1988-02-28','Pronssi','Calgary 1988', (SELECT urheilija2.id FROM urheilija2 WHERE etunimi='Marja-Liisa')),
('1980-02-24','Pronssi','Lake Placid 1980', (SELECT urheilija2.id FROM urheilija2 WHERE etunimi='Harri')),
('1988-02-19','Pronssi','Sarajevo 1984', (SELECT urheilija2.id FROM urheilija2 WHERE etunimi='Harri')),
('1984-08-12','Hopea','Los Angeles 1984', (SELECT urheilija2.id FROM urheilija2 WHERE etunimi='Ilse')),
('1992-08-09','Hopea','Barcelona 1992', (SELECT urheilija2.id FROM urheilija2 WHERE etunimi='Seppo'));